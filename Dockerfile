FROM node:12.18-alpine

WORKDIR /app

RUN npm install -g pm2
#RUN npm install -g @nestjs/cli
#RUN npm install webpack

COPY ["package.json", "package-lock.json*", "./"]

RUN npm install

COPY . .

# Development
CMD ["npm", "start"]

# Production
# CMD ["pm2-runtime", "ecosystem.config.js", "--env", "development"]
