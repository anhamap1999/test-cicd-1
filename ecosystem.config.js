const SERVER_USER = process.env.SERVER_USER
  ? process.env.SERVER_USER.trim()
  : 'ubuntu';
const SERVER_HOST = process.env.SERVER_HOST
  ? process.env.SERVER_HOST.trim()
  : '54.226.137.242';
const REPO = 'git@gitlab.com:anhamap1999/test-cicd-1.git';

module.exports = {
  apps: [
    {
      name: 'backend',
      script: 'npm run start',
      exec_mode: 'cluster',
      env: {
        NODE_ENV: 'development',
        PORT: 5003,
      },
      env_development: {
        NODE_ENV: 'development',
        PORT: 5003,
      },
    },
  ],
  deploy: {
    development: {
      user: SERVER_USER,
      host: [SERVER_HOST],
      ref: 'origin/master',
      repo: REPO,
      ssh_options: 'StrictHostKeyChecking=no',
      path: '/home/ubuntu/test-cicd1',
      'post-setup': 'npm install; npm build; pm2 start ecosystem.config.js --env development',
      'post-deploy':
        'npm install &&' +
        ' && pm2 startOrRestart ecosystem.config.js --env development' +
        ' && pm2 save',
    },
  },
};
